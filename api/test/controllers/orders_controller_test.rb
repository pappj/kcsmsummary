require 'test_helper'

class OrdersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @order = orders(:one)
  end

  test "should get index" do
    get orders_url, as: :json
    assert_response :success
  end

  test "should create order" do
    assert_difference('Order.count') do
      post orders_url, params: { order: { buyer: @order.buyer, completion_date: @order.completion_date, notes: @order.notes, order_date: @order.order_date, paid_amount: @order.paid_amount, payment_method: @order.payment_method, platform: @order.platform, price: @order.price, shipping_cost: @order.shipping_cost, shipping_mod: @order.shipping_mod, status: @order.status } }, as: :json
    end

    assert_response 201
  end

  test "should show order" do
    get order_url(@order), as: :json
    assert_response :success
  end

  test "should update order" do
    patch order_url(@order), params: { order: { buyer: @order.buyer, completion_date: @order.completion_date, notes: @order.notes, order_date: @order.order_date, paid_amount: @order.paid_amount, payment_method: @order.payment_method, platform: @order.platform, price: @order.price, shipping_cost: @order.shipping_cost, shipping_mod: @order.shipping_mod, status: @order.status } }, as: :json
    assert_response 200
  end

  test "should destroy order" do
    assert_difference('Order.count', -1) do
      delete order_url(@order), as: :json
    end

    assert_response 204
  end
end
