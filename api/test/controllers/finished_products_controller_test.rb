require 'test_helper'

class FinishedProductsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @finished_product = finished_products(:one)
  end

  test "should get index" do
    get finished_products_url, as: :json
    assert_response :success
  end

  test "should create finished_product" do
    assert_difference('FinishedProduct.count') do
      post finished_products_url, params: { finished_product: { cost: @finished_product.cost } }, as: :json
    end

    assert_response 201
  end

  test "should show finished_product" do
    get finished_product_url(@finished_product), as: :json
    assert_response :success
  end

  test "should update finished_product" do
    patch finished_product_url(@finished_product), params: { finished_product: { cost: @finished_product.cost } }, as: :json
    assert_response 200
  end

  test "should destroy finished_product" do
    assert_difference('FinishedProduct.count', -1) do
      delete finished_product_url(@finished_product), as: :json
    end

    assert_response 204
  end
end
