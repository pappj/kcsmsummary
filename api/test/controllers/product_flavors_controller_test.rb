require 'test_helper'

class ProductFlavorsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @product_flavor = product_flavors(:one)
  end

  test "should get index" do
    get product_flavors_url, as: :json
    assert_response :success
  end

  test "should create product_flavor" do
    assert_difference('ProductFlavor.count') do
      post product_flavors_url, params: { product_flavor: { product_id: @product_flavor.product_id } }, as: :json
    end

    assert_response 201
  end

  test "should show product_flavor" do
    get product_flavor_url(@product_flavor), as: :json
    assert_response :success
  end

  test "should update product_flavor" do
    patch product_flavor_url(@product_flavor), params: { product_flavor: { product_id: @product_flavor.product_id } }, as: :json
    assert_response 200
  end

  test "should destroy product_flavor" do
    assert_difference('ProductFlavor.count', -1) do
      delete product_flavor_url(@product_flavor), as: :json
    end

    assert_response 204
  end
end
