require 'test_helper'

class ProductPicturesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @product_picture = product_pictures(:one)
  end

  test "should get index" do
    get product_pictures_url, as: :json
    assert_response :success
  end

  test "should create product_picture" do
    assert_difference('ProductPicture.count') do
      post product_pictures_url, params: { product_picture: { picture: @product_picture.picture, product_flavor_id: @product_picture.product_flavor_id } }, as: :json
    end

    assert_response 201
  end

  test "should show product_picture" do
    get product_picture_url(@product_picture), as: :json
    assert_response :success
  end

  test "should update product_picture" do
    patch product_picture_url(@product_picture), params: { product_picture: { picture: @product_picture.picture, product_flavor_id: @product_picture.product_flavor_id } }, as: :json
    assert_response 200
  end

  test "should destroy product_picture" do
    assert_difference('ProductPicture.count', -1) do
      delete product_picture_url(@product_picture), as: :json
    end

    assert_response 204
  end
end
