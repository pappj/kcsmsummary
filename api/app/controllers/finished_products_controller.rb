class FinishedProductsController < ApplicationController
  before_action :set_finished_product, only: [:show, :update, :destroy]

  # GET /finished_products
  def index
    products = if params.has_key?(:product_flavor_id)
      FinishedProduct.where(product_flavor_id: params[:product_flavor_id])
    else
      FinishedProduct.all
    end

    render json: products
  end

  # GET /finished_products/1
  def show
    render json: @finished_product
  end

  # POST /finished_products
  def create
    @finished_product = FinishedProduct.new(finished_product_params)

    if @finished_product.save
      render json: @finished_product, status: :created, location: @finished_product
    else
      render json: @finished_product.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /finished_products/1
  def update
    if @finished_product.update(finished_product_params)
      render json: @finished_product
    else
      render json: @finished_product.errors, status: :unprocessable_entity
    end
  end

  # DELETE /finished_products/1
  def destroy
    @finished_product.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_finished_product
      @finished_product = FinishedProduct.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def finished_product_params
      params.require(:finished_product).permit(:cost, :product_flavor_id,
                                               :order_id)
    end
end
