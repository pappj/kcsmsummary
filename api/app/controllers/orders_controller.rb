class OrdersController < ApplicationController
  before_action :set_order, only: [:show, :update, :destroy]

  # GET /orders
  def index
    orders = Order.all
    product_ids = orders.map(&:finished_product_ids).flatten
    products = FinishedProduct.find(product_ids)
    flavor_ids = products.map(&:product_flavor_id).to_set.to_a
    flavors = ProductFlavor.find(flavor_ids)

    serializer = ActiveModelSerializers::SerializableResource
    render json: {orders: orders.map{|o| serializer.new(o)},
                  products: products.map{|p| serializer.new(p)},
                  flavors: flavors.map{|f| serializer.new(f)}}
  end

  # GET /orders/1
  def show
    render json: @order
  end

  # POST /orders
  def create
    @order = Order.new(order_params)

    if @order.save
      render json: @order, status: :created, location: @order
    else
      render json: @order.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /orders/1
  def update
    if @order.update(order_params)
      render json: @order
    else
      render json: @order.errors, status: :unprocessable_entity
    end
  end

  # DELETE /orders/1
  def destroy
    FinishedProduct.where(order: @order).update_all(order_id: nil)
    @order.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_order
      @order = Order.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def order_params
      params.require(:order).permit(:order_date, :platform, :buyer, :price,
                                    :shipping_mode, :shipping_cost, :payment_method,
                                    :notes, :completion_date, :paid_amount,
                                    :made, :paid, :dispatched, :completed,
                                    finished_product_ids: [])
    end
end
