class ApplicationController < ActionController::API
  def serialize(object)
    ActiveModelSerializers::SerializableResource.new(object)
  end
end
