class ProductFlavorsController < ApplicationController
  before_action :set_product_flavor, only: [:show, :update, :destroy]

  # GET /product_flavors
  def index
    @product_flavors = ProductFlavor.all

    render json: @product_flavors
  end

  # GET /product_flavors/1
  def show
    render json: @product_flavor
  end

  # POST /product_flavors
  def create
    @product_flavor = ProductFlavor.new(product_flavor_params)

    if @product_flavor.save
      render json: @product_flavor, status: :created, location: @product_flavor
    else
      render json: @product_flavor.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /product_flavors/1
  def update
    if @product_flavor.update(product_flavor_params)
      render json: @product_flavor
    else
      render json: @product_flavor.errors, status: :unprocessable_entity
    end
  end

  # DELETE /product_flavors/1
  def destroy
    @product_flavor.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_product_flavor
      @product_flavor = ProductFlavor.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def product_flavor_params
      params.require(:product_flavor).permit(:product_id,
        product_pictures_attributes: [:id, :picture])
    end
end
