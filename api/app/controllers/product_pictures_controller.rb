class ProductPicturesController < ApplicationController
  before_action :set_product_picture, only: [:show, :update, :destroy]

  # GET /product_pictures
  def index
    @product_pictures = ProductPicture.all

    render json: @product_pictures
  end

  # GET /product_pictures/1
  def show
    render json: @product_picture
  end

  # POST /product_pictures
  def create
    @product_picture = ProductPicture.new(product_picture_params)

    if @product_picture.save
      render json: @product_picture, status: :created, location: @product_picture
    else
      render json: @product_picture.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /product_pictures/1
  def update
    if @product_picture.update(product_picture_params)
      render json: @product_picture
    else
      render json: @product_picture.errors, status: :unprocessable_entity
    end
  end

  # DELETE /product_pictures/1
  def destroy
    @product_picture.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_product_picture
      @product_picture = ProductPicture.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def product_picture_params
      params.require(:product_picture).permit(:picture, :product_flavor_id)
    end
end
