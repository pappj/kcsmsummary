class ProductPicture < ApplicationRecord
  belongs_to :product_flavor

  mount_base64_uploader :picture, PictureUploader
end
