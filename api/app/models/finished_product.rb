class FinishedProduct < ApplicationRecord
  belongs_to :product_flavor
  belongs_to :order, optional: true

  def product
    product_flavor
  end
end
