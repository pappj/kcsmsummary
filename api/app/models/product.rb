class Product < ApplicationRecord
  belongs_to :product_category
  has_many :product_flavors

  accepts_nested_attributes_for :product_flavors

  def category
    product_category
  end
end
