class ProductFlavor < ApplicationRecord
  belongs_to :product
  has_many :product_pictures
  has_many :finished_products

  accepts_nested_attributes_for :product_pictures

  def pictures
    product_pictures
  end
end
