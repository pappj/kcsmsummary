class ExpenseSerializer < ActiveModel::Serializer
  attributes :id, :category, :date, :amount, :note
end
