class ProductPictureSerializer < ActiveModel::Serializer
  attributes :id, :picture
  has_one :product_flavor

  def picture
    object.picture.url
  end
end
