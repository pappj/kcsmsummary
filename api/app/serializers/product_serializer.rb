class ProductSerializer < ActiveModel::Serializer
  attributes :id, :name
  has_many :product_flavors, key: :flavors
end
