class OrderSerializer < ActiveModel::Serializer
  attributes :id, :order_date, :platform, :buyer, :price, :shipping_mode,
    :shipping_cost, :payment_method, :notes, :completion_date, :paid_amount,
    :made, :paid, :dispatched, :completed, :products

  def products
    object.finished_product_ids
  end
end
