class ProductFlavorSerializer < ActiveModel::Serializer
  attributes :id, :pictures

  def pictures
    object.product_pictures.map{|pic| {orig: pic.picture.original.url,
                                       thumb: pic.picture.thumbnail.url}}
  end
end
