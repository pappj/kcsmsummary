class FinishedProductSerializer < ActiveModel::Serializer
  attributes :id, :cost, :order_id, :flavor_id

  def flavor_id
    object.product_flavor_id
  end
end
