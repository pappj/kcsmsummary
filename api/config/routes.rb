Rails.application.routes.draw do
  resources :expenses
  resources :orders
  resources :finished_products
  resources :product_pictures
  resources :product_flavors
  resources :products
  resources :product_categories
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
