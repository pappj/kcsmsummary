class CreateOrders < ActiveRecord::Migration[5.2]
  def change
    create_table :orders do |t|
      t.date :order_date
      t.string :platform
      t.string :buyer
      t.integer :price
      t.string :shipping_mod
      t.integer :shipping_cost
      t.string :payment_method
      t.string :status
      t.text :notes
      t.date :completion_date
      t.integer :paid_amount

      t.timestamps
    end
  end
end
