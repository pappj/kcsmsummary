class AddFieldsToOrders < ActiveRecord::Migration[5.2]
  def change
    add_column :orders, :made, :boolean, default: false
    add_column :orders, :paid, :boolean, default: false
    add_column :orders, :dispatched, :boolean, default: false
    add_column :orders, :completed, :boolean, default: false
  end
end
