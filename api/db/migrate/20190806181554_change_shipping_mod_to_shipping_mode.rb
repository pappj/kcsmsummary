class ChangeShippingModToShippingMode < ActiveRecord::Migration[5.2]
  def change
    rename_column :orders, :shipping_mod, :shipping_mode
  end
end
