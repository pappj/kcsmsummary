class AddOrderToFinishedProducts < ActiveRecord::Migration[5.2]
  def change
    add_reference :finished_products, :order, foreign_key: true
  end
end
