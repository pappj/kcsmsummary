class CreateFinishedProducts < ActiveRecord::Migration[5.2]
  def change
    create_table :finished_products do |t|
      t.integer :cost

      t.timestamps
    end
  end
end
