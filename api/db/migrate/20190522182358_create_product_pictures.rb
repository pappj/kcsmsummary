class CreateProductPictures < ActiveRecord::Migration[5.2]
  def change
    create_table :product_pictures do |t|
      t.string :picture
      t.references :product_flavor, foreign_key: true

      t.timestamps
    end
  end
end
