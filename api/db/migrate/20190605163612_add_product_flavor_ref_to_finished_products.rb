class AddProductFlavorRefToFinishedProducts < ActiveRecord::Migration[5.2]
  def change
    add_reference :finished_products, :product_flavor, foreign_key: true
  end
end
