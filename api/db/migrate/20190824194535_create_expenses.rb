class CreateExpenses < ActiveRecord::Migration[5.2]
  def change
    create_table :expenses do |t|
      t.string :type
      t.date :date
      t.integer :amount
      t.text :note

      t.timestamps
    end
  end
end
