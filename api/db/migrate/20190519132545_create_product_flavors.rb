class CreateProductFlavors < ActiveRecord::Migration[5.2]
  def change
    create_table :product_flavors do |t|
      t.references :product, foreign_key: true

      t.timestamps
    end
  end
end
