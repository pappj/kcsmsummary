# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_08_24_204606) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "expenses", force: :cascade do |t|
    t.string "category"
    t.date "date"
    t.integer "amount"
    t.text "note"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "finished_products", force: :cascade do |t|
    t.integer "cost"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "product_flavor_id"
    t.bigint "order_id"
    t.index ["order_id"], name: "index_finished_products_on_order_id"
    t.index ["product_flavor_id"], name: "index_finished_products_on_product_flavor_id"
  end

  create_table "orders", force: :cascade do |t|
    t.date "order_date"
    t.string "platform"
    t.string "buyer"
    t.integer "price"
    t.string "shipping_mode"
    t.integer "shipping_cost"
    t.string "payment_method"
    t.text "notes"
    t.date "completion_date"
    t.integer "paid_amount"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "made", default: false
    t.boolean "paid", default: false
    t.boolean "dispatched", default: false
    t.boolean "completed", default: false
  end

  create_table "product_categories", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "product_flavors", force: :cascade do |t|
    t.bigint "product_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["product_id"], name: "index_product_flavors_on_product_id"
  end

  create_table "product_pictures", force: :cascade do |t|
    t.string "picture"
    t.bigint "product_flavor_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["product_flavor_id"], name: "index_product_pictures_on_product_flavor_id"
  end

  create_table "products", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "product_category_id"
    t.index ["product_category_id"], name: "index_products_on_product_category_id"
  end

  add_foreign_key "finished_products", "orders"
  add_foreign_key "finished_products", "product_flavors"
  add_foreign_key "product_flavors", "products"
  add_foreign_key "product_pictures", "product_flavors"
  add_foreign_key "products", "product_categories"
end
