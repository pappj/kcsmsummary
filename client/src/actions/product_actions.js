import axios from "axios";
import { normalize, schema } from "normalizr";

import { receiveFlavors } from "./flavor_actions.js";

export const REQUEST_PRODUCTS = "REQUEST_PRODUCTS";
export const RECEIVE_PRODUCTS = "RECEIVE_PRODUCTS";
export const ADD_PRODUCT      = "ADD_PRODUCT";
export const UPDATE_PRODUCT   = "UPDATE_PRODUCT";
export const RECEIVE_SINGLE_PRODUCT = "RECEIVE_SINGLE_PRODUCT";

export function requestProducts(categoryId) {
  return {
    type: REQUEST_PRODUCTS,
    categoryId
  };
}

export function receiveProducts(categoryId, products, newIds) {
  return {
    type: RECEIVE_PRODUCTS,
    categoryId,
    products,
    newIds
  };
}

export function addProduct(categoryId, product) {
  return {
    type: ADD_PRODUCT,
    categoryId,
    product
  };
}

export function updateProduct(id, product) {
  return {
    type: UPDATE_PRODUCT,
    id,
    product
  };
}

export function receiveSingleProduct(product) {
  return {
    type: RECEIVE_SINGLE_PRODUCT,
    product
  };
}

const flavor = new schema.Entity("flavors");
const productEntity = new schema.Entity("products", { flavors: [flavor] });
const productSchema = [productEntity];

export function fetchProducts(categoryId) {
  return function(dispatch, getState) {
    if(!getState().categories[categoryId].products) {
      dispatch(requestProducts(categoryId));
      axios.get("/api/products?product_category_id=" + categoryId)
        .then((response) => {
          const data = normalize(response.data, productSchema);
          dispatch(receiveFlavors(data.entities.flavors));
          dispatch(receiveProducts(categoryId, data.entities.products, data.result));
        });
    }
  }
}

export function fetchSingleProduct(productId) {
  return function(dispatch, getState) {
    if(!getState().products[productId]) {
      return axios.get("/api/products/" + productId)
        .then((response) => {
          const data = normalize(response.data, productEntity);
          const newProduct = Object.values(data.entities.products)[0];
          dispatch(receiveFlavors(data.entities.flavors));
          dispatch(receiveSingleProduct(newProduct));
        });
    } else {
      return Promise.resolve();
    }
  };
}

export function createProduct(productToPost) {
  return function(dispatch) {
    return axios.post("/api/products", {product: productToPost})
      .then((response) => {
        const categoryId = productToPost.product_category_id;
        const data = normalize(response.data, productEntity);
        const newProduct = Object.values(data.entities.products)[0];
        dispatch(receiveFlavors(data.entities.flavors));
        dispatch(addProduct(categoryId, newProduct));
      });
  };
}
