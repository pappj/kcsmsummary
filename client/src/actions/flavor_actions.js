import axios from "axios";

export const RECEIVE_FLAVORS = "RECEIVE_FLAVORS";
export const ADD_FLAVOR = "ADD_FLAVOR";
export const UPDATE_FLAVOR = "UPDATE_FLAVOR";

export function receiveFlavors(flavors) {
  return {
    type: RECEIVE_FLAVORS,
    flavors
  };
}

export function addFlavor(productId, flavor) {
  return {
    type: ADD_FLAVOR,
    productId,
    flavor
  };
}

export function updateFlavor(id, flavor) {
  return {
    type: UPDATE_FLAVOR,
    id,
    flavor
  };
}

export function createFlavor(flavorToPost) {
  return function(dispatch) {
    return axios.post("/api/product_flavors", {product_flavor: flavorToPost})
      .then((response) => {
        dispatch(addFlavor(flavorToPost.product_id, response.data));
      });
  };
}
