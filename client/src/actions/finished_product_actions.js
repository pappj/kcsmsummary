import axios from "axios";
import { normalize, schema } from "normalizr";

export const REQUEST_FINISHED_PRODUCTS = "REQUEST_FINISHED_PRODUCTS";
export const RECEIVE_FINISHED_PRODUCTS = "RECEIVE_FINISHED_PRODUCTS";
export const RECEIVE_ONLY_FINISHED_PRODUCTS = "RECEIVE_ONLY_FINISHED_PRODUCTS";
export const ADD_FINISHED_PRODUCT = "ADD_FINISHED_PRODUCT";
export const UPDATE_FINISHED_PRODUCT = "UPDATE_FINISHED_PRODUCT";
export const UPDATE_FINISHED_PRODUCT_ORDERS = "UPDATE_FINISHED_PRODUCT_ORDERS";

export function requestFinishedProducts(flavorId) {
  return {
    type: REQUEST_FINISHED_PRODUCTS,
    flavorId
  };
}

export function receiveFinishedProducts(flavorId, finishedProducts, newIds) {
  return {
    type: RECEIVE_FINISHED_PRODUCTS,
    flavorId,
    finishedProducts,
    newIds
  };
}

export function receiveOnlyFinishedProducts(finishedProducts) {
  return {
    type: RECEIVE_ONLY_FINISHED_PRODUCTS,
    finishedProducts
  };
}

export function addFinishedProduct(flavorId, finishedProduct) {
  return {
    type: ADD_FINISHED_PRODUCT,
    flavorId,
    finishedProduct
  };
}

export function updateFinishedProduct(finishedProduct) {
  return {
    type: UPDATE_FINISHED_PRODUCT,
    finishedProduct
  };
}

export function updateFinishedProductOrders(orderId, addedProducts,
                                            removedProducts) {
  return {
    type: UPDATE_FINISHED_PRODUCT_ORDERS,
    orderId,
    addedProducts,
    removedProducts
  };
}

const finishedProductEntity = new schema.Entity("finishedProducts");
const finishedProductSchema = [finishedProductEntity];

export function fetchFinishedProducts(flavorId) {
  return function(dispatch, getState) {
    if(!getState().flavors[flavorId].finishedProducts) {
      dispatch(requestFinishedProducts(flavorId));
      axios.get("/api/finished_products?product_flavor_id=" + flavorId)
        .then((response) => {
          const data = normalize(response.data, finishedProductSchema);
          dispatch(receiveFinishedProducts(flavorId,
                                           data.entities.finishedProducts,
                                           data.result));
        });
    }
  }
}

export function createFinishedProduct(productToPost) {
  return function(dispatch) {
    return axios.post("/api/finished_products", {finished_product: productToPost})
      .then((response) => {
        const flavorId = productToPost.product_flavor_id;
        dispatch(addFinishedProduct(flavorId, response.data));
      });
  };
}

export function patchFinishedProduct(id, updatedParams) {
  return function(dispatch) {
    return axios.patch("/api/finished_products/" + id,
                       {finished_product: {...updatedParams}})
      .then((response) => {
        const data = normalize(response.data, finishedProductEntity);
        const updatedProduct = Object.values(data.entities.finishedProducts)[0];
        dispatch(updateFinishedProduct(updatedProduct));
      });
  };
}

