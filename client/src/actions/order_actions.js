import axios from "axios";
import { normalize, schema } from "normalizr";

import { receiveOnlyFinishedProducts } from "./finished_product_actions.js";
import { receiveFlavors } from "./flavor_actions.js";

export const REQUEST_ORDERS = "REQUEST_ORDERS";
export const RECEIVE_ORDERS = "RECEIVE_ORDERS";
export const ADD_ORDER      = "ADD_ORDER";
export const UPDATE_ORDER   = "UPDATE_ORDER";
export const DELETE_ORDER   = "DELETE_ORDER";

export function requestOrders() {
  return {
    type: REQUEST_ORDERS
  };
}

export function receiveOrders(orders) {
  return {
    type: RECEIVE_ORDERS,
    orders
  };
}

export function addOrder(order) {
  return {
    type: ADD_ORDER,
    order
  };
}

export function updateOrder(order) {
  return {
    type: UPDATE_ORDER,
    order
  };
}

export function deleteOrder(orderId) {
  return {
    type: DELETE_ORDER,
    id: orderId
  };
}

const order = new schema.Entity("orders");
const product = new schema.Entity("products");
const flavor = new schema.Entity("flavors");
const orderSchema = {orders: [order], products: [product], flavors: [flavor]};

export function fetchOrders() {
  return function(dispatch, getState) {
    if(!getState().loadingOrders && !getState().ordersLoaded) {
      dispatch(requestOrders());
      return axios.get("/api/orders")
        .then((response) => {
          const data = normalize(response.data, orderSchema);
          dispatch(receiveOnlyFinishedProducts(data.entities.products));
          dispatch(receiveFlavors(data.entities.flavors));
          dispatch(receiveOrders(data.entities.orders));
        });
    } else {
      return Promise.resolve();
    }
  };
}

export function createOrder(orderToPost) {
  return function(dispatch) {
    return axios.post("/api/orders", {order: orderToPost})
      .then((response) => {
        const data = normalize(response.data, order);
        const newOrder = Object.values(data.entities.orders)[0];
        dispatch(addOrder(newOrder));
      });
  };
}

export function patchOrder(id, updatedParams) {
  return function(dispatch) {
    return axios.patch("/api/orders/" + id, {order: {...updatedParams}})
      .then((response) => {
        const data = normalize(response.data, order);
        const updatedOrder = Object.values(data.entities.orders)[0];
        dispatch(updateOrder(updatedOrder));
      });
  };
}

export function removeOrder(id) {
  return function(dispatch) {
    return axios.delete("/api/orders/" + id)
      .then((response) => dispatch(deleteOrder(id)));
  };
}
