import axios from "axios";
import { normalize, schema } from "normalizr";

export const REQUEST_EXPENSES = "REQUEST_EXPENSES";
export const RECEIVE_EXPENSES = "RECEIVE_EXPENSES";
export const ADD_EXPENSE = "ADD_EXPENSE";
export const UPDATE_EXPENSE = "UPDATE_EXPENSE";
export const DELETE_EXPENSE = "DELETE_EXPENSE";

export function requestExpenses() {
  return {
    type: REQUEST_EXPENSES
  };
}

export function receiveExpenses(expenses) {
  return {
    type: RECEIVE_EXPENSES,
    expenses
  };
}

export function addExpense(expense) {
  return {
    type: ADD_EXPENSE,
    expense
  };
}

export function updateExpense(expense) {
  return {
    type: UPDATE_EXPENSE,
    expense
  };
}

export function deleteExpense(expenseId) {
  return {
    type: DELETE_EXPENSE,
    id: expenseId
  };
}

const expenseEntity = new schema.Entity("expenses");
const expenseSchema = [expenseEntity];

export function fetchExpenses() {
  return function(dispatch, getState) {
    if(!getState().expensesLoaded && !getState().loadingExpenses) {
      dispatch(requestExpenses());
      return axios.get("/api/expenses/")
        .then((response) => {
          const data = normalize(response.data, expenseSchema);
          dispatch(receiveExpenses(data.entities.expenses));
        });
    } else {
      return Promise.resolve();
    }
  };
}

export function createExpense(expenseToPost) {
  return function(dispatch) {
    return axios.post("/api/expenses", {expense: expenseToPost})
      .then((response) => {
        dispatch(addExpense(response.data));
      });
  };
}

export function patchExpense(expense) {
  return function(dispatch) {
    return axios.patch("/api/expenses/" + expense.id, {expense})
      .then((response) => dispatch(updateExpense(response.data)));
  };
}

export function removeExpense(expenseId) {
  return function(dispatch) {
    return axios.delete("/api/expenses/" + expenseId)
      .then((response) => dispatch(deleteExpense(expenseId)));
  };
}
