import axios from "axios";
import { normalize, schema } from "normalizr";

export const REQUEST_CATEGORIES = "REQUEST_CATEGORIES";
export const RECEIVE_CATEGORIES = "RECEIVE_CATEGORIES";
export const ADD_CATEGORY       = "ADD_CATEGORY";
export const UPDATE_CATEGORY    = "UPDATE_CATEGORY";

export function requestCategories() {
  return {
    type: REQUEST_CATEGORIES
  };
}

export function receiveCategories(categories) {
  return {
    type: RECEIVE_CATEGORIES,
    categories
  };
}

export function addCategory(category) {
  return {
    type: ADD_CATEGORY,
    category
  };
}

export function updateCategory(id, category) {
  return {
    type: UPDATE_CATEGORY,
    id,
    category
  };
}

const category = new schema.Entity("categories");
const categorySchema = [category];

export function fetchCategories() {
  return function(dispatch, getState) {
    if(!getState().categoriesLoaded && !getState().loadingCategories) {
      dispatch(requestCategories());
      axios.get("/api/product_categories")
        .then((response) => {
          const data = normalize(response.data, categorySchema);
          dispatch(receiveCategories(data.entities.categories));
        });
    }
  }
}

