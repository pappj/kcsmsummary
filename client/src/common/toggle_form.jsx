import React from "react";
import Button from "react-bootstrap/Button.js";
import GenericForm from "./generic_form.jsx";

export default class ToggleForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showForm: false
    };

    this.toggle = this.toggle.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  toggle() {
    this.setState({showForm: !this.state.showForm});
  }

  onSubmit(instance) {
    this.setState({showForm: false});
    this.props.onSubmit(instance);
  }

  render() {
    let button;
    let form;
    if(this.state.showForm) {
      button = <Button variant="primary" onClick={this.toggle}>Mégse</Button>;
      form = <GenericForm instance={this.props.instance} fields={this.props.fields}
                onSubmit={this.onSubmit} submitLabel={this.props.submitLabel}
                children={this.props.children} />;
    } else {
      button = <Button variant="primary" onClick={this.toggle}>
                 {this.props.toggleLabel}</Button>;
    }

    return <div>
      {button}
      {form}
    </div>;
  }
}
