export function encodeBase64(file) {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = error => reject(error);
  });
}

export function getParamFromProps(props, name) {
  const value = new URLSearchParams(props.location.search).get(name);
  return value;
}
