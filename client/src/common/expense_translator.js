export function translate(phrase) {
  switch(phrase) {
    case "accountant":
      return "Könyvelő";
    case "administrative":
      return "Vállalkozás";
    case "commodity":
      return "Alapanyag";
    case "decorand":
      return "Decorand";
    case "eurosuv":
      return "Euro Suveniers";
    case "foxpost":
      return "FoxPost";
    case "gls":
      return "GLS";
    case "ipa":
      return "Iparúzési adó";
    case "kata":
      return "KATA";
    case "kosarbolt":
      return "Kosárbolt";
    case "kreativbirodalom":
      return "Kreatív birodalom";
    case "medves":
      return "Medvés nagyker";
    case "other":
      return "Egyéb";
    case "postage":
      return "Postaköltség";
    case "qx":
      return "QX-Impex";
    case "remenyi":
      return "Reményi";
    case "szamla":
      return "Számlatömb";
    case "szarazvirag":
      return "Szárazvirág";
    case "viragtrend":
      return "VirágTrend";
    case "wanapack":
      return "Wanapack";
    case "wrapper":
      return "Csomagolóanyag";
  }
}

export function listToDictionary(list) {
  return list.reduce((acc, word) => {return {...acc,
                                             [word]: translate(word)};},
                     {});
}
