import React from "react";
import Form from "react-bootstrap/Form.js";
import Button from "react-bootstrap/Button.js";
import Row from "react-bootstrap/Row.js";
import Col from "react-bootstrap/Col.js";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

export default class GenericForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      instance: props.instance
    };

    this.changeDate = this.changeDate.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.addFileToList = this.addFileToList.bind(this);
    this.deleteFromList = this.deleteFromList.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.renderField = this.renderField.bind(this);
    this.renderSelect = this.renderSelect.bind(this);
  }

  changeDate(date, fieldName) {
    this.setState({
      instance: {...this.state.instance, [fieldName]: date}
    });
  }

  handleChange(evt) {
    evt.preventDefault();
    this.setState({
      instance: {...this.state.instance, [evt.target.id]: evt.target.value}
    });
  }

  addFileToList(field, evt) {
    evt.preventDefault();

    const instance = this.state.instance;
    this.setState({
      instance: {...instance,
                 [field]: [...instance[field], evt.target.files[0]]}
    });
  }

  deleteFromList(field, idx) {
    const instance = this.state.instance;
    this.setState({
      instance: {...instance,
                 [field]: [...instance[field].slice(0, idx),
                           ...instance[field].slice(idx+1)]}
    });
  }

  onSubmit(evt) {
    evt.preventDefault();
    this.props.onSubmit(this.state.instance);
  }

  renderField(field) {
    const instance = this.state.instance;
    let control;
    switch(field.type) {
      case "date":
        let date = new Date();
        if(this.state.instance[field.name]) {
          date = new Date(this.state.instance[field.name]);
        }

        control = <Col sm="9">
          <DatePicker dateFormat="yyyy.MM.dd" className="form-control"
            onChange={(date) => this.changeDate(date, field.name)}
            selected={date} />
        </Col>;
        break;
      case "select":
        control = this.renderSelect(field, field.options);
        break;
      case "dependentSelect":
        let options = Object.values(field.options)[0];
        if(instance[field.dependsOn]) {
          options = field.options[instance[field.dependsOn]];
        }

        control = this.renderSelect(field, options);
        break;
      case "images":
        control = <Col sm="9">
          {this.state.instance[field.name].map((image, i) => {
            return <div key={i} className="img-thumb-container">
              <img src={URL.createObjectURL(image)} className="img-fluid" />
              <button className="delete-img"
                onClick={(evt) => {evt.preventDefault();
                                   this.deleteFromList(field.name, i)}}>X</button>
            </div>;
          })}
          <Form.Label className="add-img-icon">+</Form.Label>
          <Form.Control className="sr-only" type="file"
            onChange={(evt) => this.addFileToList(field.name, evt)} />
        </Col>;
        break;
      default:
        let val = "";
        if(instance[field.name]) {
          val = instance[field.name];
        }

        control = <Col sm="9">
          <Form.Control type={field.type} value={val}
            onChange={this.handleChange} />
        </Col>;
    }

    return <Form.Group as={Row} key={field.name} controlId={field.name}>
      <Form.Label column sm="3">{field.label}</Form.Label>
      {control}
    </Form.Group>;
  }

  renderSelect(field, options) {
    let selected = options[0];
    const instance = this.state.instance;

    if(instance[field.name]) {
      selected = instance[field.name];
    }

    return <Col sm="9">
      <Form.Control as="select" value={selected} onChange={this.handleChange}>
        {Object.keys(options).map((opt, idx) => {
          return <option key={idx} value={opt}>{options[opt]}</option>;
        })}
      </Form.Control>
    </Col>;
  }

  render() {
    return <Form onSubmit={this.onSubmit}>
      {this.props.fields.map(this.renderField)}
      {this.props.children}
      <Form.Group as={Row}>
        <Button type="submit" variant="primary">{this.props.submitLabel}</Button>
      </Form.Group>
    </Form>;
  }
}
