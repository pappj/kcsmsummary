import axios from "axios";
import React from "react";
import Button from "react-bootstrap/Button.js";
import Row from "react-bootstrap/Row.js";
import Col from "react-bootstrap/Col.js";
import Form from "react-bootstrap/Form.js";
import ToggleForm from "../common/toggle_form.jsx";

import ProductSelector from "./product_selector/product_selector.jsx";

export default class OrderForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selectingProduct: false,
      products: this.props.order.products
    };

    this.submitOrder = this.submitOrder.bind(this);
    this.addProduct = this.addProduct.bind(this);

    this.formFields = [
      { name: "order_date",
        type: "date",
        label: "Megrendelés dátuma"},
      { name: "platform",
        type: "select",
        label: "Platform",
        options: {
          facebook: "Facebook",
          meska: "Meska",
          marketplace: "Marketplace",
          insta: "Instagram",
          other: "Egyéb"
        }},
      { name: "buyer",
        type: "text",
        label: "Vevő"},
      { name: "price",
        type: "number",
        label: "Meghirdetett ár"},
      { name: "shipping_mode",
        type: "select",
        label: "Szállítási mód",
        options: {
          person: "Személyes átvétel",
          foxpost: "FoxPost",
          gls: "GLS",
          other: "Egyéb"
        }},
      { name: "shipping_cost",
        type: "number",
        label: "Szállítási költség"},
      { name: "payment_method",
        type: "select",
        label: "Fizetési mód",
        options: {
          cash: "Készpénz",
          bank: "Átutalás",
          barion: "Barion",
          cod: "Utánvét",
          other: "Egyéb"
        }},
      { name: "notes",
        type: "textarea",
        label: "Megjegyzés"},
      { name: "completion_date",
        type: "date",
        label: "Teljesítés dátuma"},
      { name: "paid_amount",
        type: "number",
        label: "Kifizetett összeg"}
    ]
  }

  addProduct(prodId) {
    this.setState({
      products: [...this.state.products, prodId],
      selectingProduct: false
    });
  }

  selecting() {
    this.setState({selectingProduct: true});
  }

  cancelSelecting() {
    this.setState({selectingProduct: false});
  }

  deleteProduct(idx) {
    const products = this.state.products;
    this.setState({
      products: [...products.slice(0, idx),
                 ...products.slice(idx+1)]
    });
  }

  submitOrder(order) {
    this.props.submitOrder({...order,
                            finished_product_ids: this.state.products})
      .then((response) => {
        this.setState({
          selectingProduct: false,
          products: this.props.order.products
        });
      })
  }

  render() {
    let newProduct;
    if(this.state.selectingProduct) {
      newProduct = <ProductSelector cancel={() => this.cancelSelecting()}
        selectFinishedProduct={this.addProduct} />;
    } else {
      newProduct = <Button variant="primary" onClick={() => this.selecting()}>
        Termék hozzáadása</Button>;
    }


    return <ToggleForm onSubmit={this.submitOrder} fields={this.formFields}
             instance={this.props.order} submitLabel="Mentés"
             toggleLabel={this.props.toggleLabel}>
      <Form.Group as={Row} controlId="products">
        <Form.Label>Termékek</Form.Label>
        {this.state.products.map((productId, idx) => {
          const product = this.props.finishedProducts[productId];
          const image = "/api" + this.props.flavors[product.flavor_id].pictures[0].thumb;
          return <Row key={productId}>
            <Col><img src={image} /></Col>
            <Col><p>{product.cost}</p></Col>
            <Col><Button variant="danger" onClick={()=>this.deleteProduct(idx)}>
                   Törlés</Button></Col>
          </Row>
        })}
      </Form.Group>
      {newProduct}
    </ToggleForm>;
  }
}
