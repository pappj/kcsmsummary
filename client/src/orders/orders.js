import { connect } from "react-redux";
import { fetchOrders, createOrder, patchOrder, removeOrder }
  from "../actions/order_actions.js";
import { updateFinishedProductOrders }
  from "../actions/finished_product_actions.js";

import OrdersView from "./orders_view.jsx";

const mapStateToProps = (state) => {
  let orders = {};
  if(state.orders) {
    orders = state.orders;
  }

  return {
    isLoading: state.loadingOrders,
    orders,
    finishedProducts: state.finishedProducts,
    flavors: state.flavors
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    loadOrders: () => dispatch(fetchOrders()),
    createOrder: (order) => dispatch(createOrder(order)),
    updateOrder: (id, updateParams) => dispatch(patchOrder(id, updateParams)),
    removeOrder: (id) => dispatch(removeOrder(id)),
    updateProducts: (orderId, addedProducts, removedProducts) =>
      dispatch(updateFinishedProductOrders(orderId, addedProducts, removedProducts))
  };
};

const Orders = connect(
  mapStateToProps,
  mapDispatchToProps
)(OrdersView);

export default Orders;
