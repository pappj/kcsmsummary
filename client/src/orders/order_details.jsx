import React from "react";
import Row from "react-bootstrap/Row.js";
import Col from "react-bootstrap/Col.js";

export default class OrderDetails extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};

    this.fields = [["order_date", "Megrendelés dátuma"],
                   ["platform", "Platform"],
                   ["buyer", "Vevő"],
                   ["price", "Meghirdetett ár"],
                   ["shipping_mode", "Szállítási mód"],
                   ["shipping_cost", "Szállítási költség"],
                   ["payment_method", "Fizetési mód"],
                   ["notes", "Megjegyzés"],
                   ["completion_date", "Teljesítés dátuma"],
                   ["paid_amount", "Fizetett összeg"]];

    this.renderField = this.renderField.bind(this);
  }

  renderField(attr, label) {
    return [<Col key={label} md="3">{label}</Col>,
            <Col key={label+"_value"} md="3">{this.props.order[attr]}</Col>];
  }

  render() {
    return <Row>
      {this.fields.map((item) => this.renderField(item[0], item[1]))}
    </Row>;
  }
}
