import React from "react";
import ListGroup from "react-bootstrap/ListGroup.js";
import Button from "react-bootstrap/Button.js";
import Row from "react-bootstrap/Row.js";
import Form from "react-bootstrap/Form.js";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

import OrderForm from "./order_form.jsx";
import OrderDetails from "./order_details.jsx";

export default class OrdersView extends React.Component {
  constructor(props) {
    super(props);

    let state;
    if(props.orders.length > 0) {
      state = {...this.separateOrders(props.orders),
               showAttributes: {}};
    } else {
      state = {
        pendingOrders: [],
        completedOrders: [],
        showAttributes: {}
      };
    }

    this.state = state;
    this.updateShowAttributes = this.updateShowAttributes.bind(this);
    this.modifyOrder = this.modifyOrder.bind(this);
    this.renderButton = this.renderButton.bind(this);
    this.renderDetailsButton = this.renderDetailsButton.bind(this);
    this.renderPaidButton = this.renderPaidButton.bind(this);
    this.renderCompletedButton = this.renderCompletedButton.bind(this);
    this.renderDeleteButton = this.renderDeleteButton.bind(this);
    this.renderPaidForm = this.renderPaidForm.bind(this);
    this.renderCompletedForm = this.renderCompletedForm.bind(this);
    this.renderDeleteForm = this.renderDeleteForm.bind(this);
  }

  componentDidMount() {
    this.props.loadOrders();
  }

  componentDidUpdate(prevProps) {
    if(prevProps !== this.props) {
      const pendingAndCompletedOrders = this.separateOrders(this.props.orders);
      this.setState(pendingAndCompletedOrders);
    }
  }

  separateOrders(orders) {
    const orderArray = Object.values(orders);
    const pendingOrders = orderArray.filter(order => !order.completed);
    const completedOrders = orderArray.filter(order => order.completed);

    return {pendingOrders, completedOrders};
  }

  updateShowAttributes(id, attrs) {
    this.setState({
      showAttributes: {...this.state.showAttributes,
                       [id]: {...this.state.showAttributes[id],
                              ...attrs}}
    });
  }

  renderButton(order, attr, label) {
    let variant = "primary";
    if(order[attr]) {
      variant = "success";
    }

    return <Button variant={variant}
      onClick={()=>this.props.updateOrder(order.id, {[attr]: !order[attr]})}>
        {label}</Button>
  }

  renderPaidButton(order) {
    let variant = "primary";
    if(order.paid) {
      variant = "success";
    }

    let click;
    const paid = order.paid;
    if(order.paid_amount && paid) {
      click = () => this.props.updateOrder(order.id, {paid: false,
                                                      paid_amount: 0});
    } else if(order.paid_amount) {
      click = () => this.props.updateOrder(order.id, {paid: true});
    } else {
      click = () => this.updateShowAttributes(order.id, {paidForm: true,
                                                         completedForm: false});
    }

    return <Button variant={variant} onClick={click}>Fizetve</Button>
  }

  renderCompletedButton(order) {
    let variant = "primary";
    if(order.completed) {
      variant = "success";
    }

    let click;
    if(order.paid_amount && order.completion_date) {
      click = () => this.props.updateOrder(order.id, {completed: !order.completed});
    } else {
      const date = order.completion_date;
      click = () => this.updateShowAttributes(order.id, {paidForm: false,
                                                         completedForm: true,
                                                         completionDate: date});
    }

    return <Button variant={variant} onClick={click}>Teljesítve</Button>
  }

  renderDetailsButton(orderId) {
    let detailsLabel = "Részletek";
    const attrs = this.state.showAttributes[orderId];
    if(attrs && attrs.details) {
      detailsLabel = "Kevesebb";
    }

    return <Button variant="info"
             onClick={()=>this.updateShowAttributes(orderId,
                                                    {details: !(attrs &&
                                                                attrs.details)})}>
               {detailsLabel}</Button>
  }

  renderDeleteButton(orderId) {
    return <Button variant="danger"
             onClick={() => this.updateShowAttributes(orderId,
                                                      {deleteForm: true})}>
               Törlés</Button>;
  }

  renderPaidForm(orderId) {
    let form;
    const attrs = this.state.showAttributes[orderId];

    if(attrs && attrs.paidForm) {
      form = <Form inline>
        <Form.Group controlId="paid_amount">
          <Form.Label srOnly="true">Fizetett összeg</Form.Label>
          <Form.Control value={attrs.paidAmount || ""} type="text"
            placeholder="Összeg"
            onChange={(evt) => {evt.preventDefault();
                                this.updateShowAttributes(orderId,
                                  {paidAmount: evt.target.value});}} />
        </Form.Group>
        <Button type="submit" variant="primary"
          onClick={(evt) => {evt.preventDefault();
                             this.updateShowAttributes(orderId,
                               {paidForm: false,
                                completedForm: false});
                             this.props.updateOrder(orderId,
                               {paid_amount: attrs.paidAmount,
                                paid: true});}}>
          Mentés</Button>
        <Button type="submit" variant="primary"
          onClick={(evt) => {evt.preventDefault();
                             this.updateShowAttributes(orderId,
                               {paidForm: false});}}>
          Mégse</Button>
      </Form>
    }

    return form;
  }

  renderCompletedForm(orderId) {
    let form;
    const attrs = this.state.showAttributes[orderId];

    if(attrs && attrs.completedForm) {
      let complDate = new Date();
      if(attrs.completionDate) {
        complDate = new Date(attrs.completionDate);
      }

      form = <Form inline>
        <Form.Group controlId="paid_amount">
          <Form.Label srOnly="true">Fizetett összeg</Form.Label>
          <Form.Control value={attrs.paidAmount || ""} type="text"
            placeholder="Összeg"
            onChange={(evt) => {evt.preventDefault();
                                this.updateShowAttributes(orderId,
                                  {paidAmount: evt.target.value});}} />
        </Form.Group>
        <Form.Group controlId="completion_date">
          <Form.Label srOnly="true">Teljesítési dátum</Form.Label>
          <DatePicker dateFormat="yyyy.MM.dd" className="form-control"
            onChange={(date) => this.updateShowAttributes(orderId,
                                  {completionDate: date})}
            selected={complDate} />
        </Form.Group>
        <Button type="submit" variant="primary"
          onClick={(evt) => {evt.preventDefault();
                             this.updateShowAttributes(orderId,
                               {paidForm: false,
                                completedForm: false});
                             this.props.updateOrder(orderId,
                               {paid_amount: attrs.paidAmount,
                                completion_date: attrs.completionDate,
                                paid: true,
                                completed: true});}}>
          Mentés</Button>
        <Button type="submit" variant="primary"
          onClick={(evt) => {evt.preventDefault();
                             this.updateShowAttributes(orderId,
                               {completedForm: false});}}>
          Mégse</Button>
      </Form>
    }

    return form;
  }

  renderDeleteForm(orderId) {
    let form;

    const attrs = this.state.showAttributes[orderId];
    if(attrs && attrs.deleteForm) {
      form =  <Row>
        <p>Tényleg törlöd ezt a megrendelést?</p>
        <Button variant="danger"
          onClick={(evt) => {evt.preventDefault();
                             this.updateShowAttributes(orderId,
                                                       {deleteForm: false});
                             this.props.removeOrder(orderId);}}>
          Igen</Button>
        <Button variant="primary"
          onClick={(evt) => {evt.preventDefault();
                             this.updateShowAttributes(orderId,
                                                       {deleteForm: false});}}>
          Mégse</Button>
      </Row>;
    }

    return form;
  }

  getDefaultOrder() {
    const today = new Date();
    return {
      order_date: today,
      platform: "facebook",
      buyer: "",
      price: "",
      shipping_mode: "person",
      shipping_cost: 0,
      payment_method: "cash",
      notes: "",
      completion_date: today,
      paid_amount: "",
      products: []
    };
  }

  modifyOrder(order) {
    const prevProducts = this.props.orders[order.id].products;
    const newProducts = order.finished_product_ids;

    const addedProducts = newProducts.filter((p) => !prevProducts.includes(p));
    const removedProducts = prevProducts.filter((p) => !newProducts.includes(p));

    console.log(prevProducts);
    console.log(newProducts);
    console.log(addedProducts);
    console.log(removedProducts);
    this.props.updateProducts(order.id, addedProducts, removedProducts);
    return this.props.updateOrder(order.id, order);
  }

  render() {
    let content;
    const updateFun = this.props.updateOrder;

    if(this.props.isLoading) {
      content = <p>Betöltés</p>;
    } else {
      content = <div>
        <h2>Folyamatban lévő rendelések</h2>
        <ListGroup as="ul">
          {this.state.pendingOrders.map((order) => {
            let details;
            const attrs = this.state.showAttributes[order.id];
            if(attrs && attrs.details) {
              details = <OrderDetails order={order} />
            }

            return <ListGroup.Item key={order.id} as="li">
              <Row>{order.buyer} ({order.platform})</Row>
              <Row>
                {this.renderButton(order, "made", "Elkészítve")}
                {this.renderPaidButton(order)}
                {this.renderButton(order, "dispatched", "Postázva")}
                {this.renderCompletedButton(order)}
                {this.renderDetailsButton(order.id)}
                {this.renderDeleteButton(order.id)}
              </Row>
              <Row>
                <OrderForm finishedProducts={this.props.finishedProducts}
                  flavors={this.props.flavors} submitOrder={this.modifyOrder}
                  toggleLabel="Módosítás" order={order} />
              </Row>
              {this.renderPaidForm(order.id)}
              {this.renderCompletedForm(order.id)}
              {this.renderDeleteForm(order.id)}
              {details}
            </ListGroup.Item>;
          })}
        </ListGroup>
        <h2>Teljesített rendelések</h2>
        <ListGroup as="ul">
          {this.state.completedOrders.map((order) => {
            let details;
            const attrs = this.state.showAttributes[order.id];
            if(attrs && attrs.details) {
              details = <OrderDetails order={order} />
            }

            return <ListGroup.Item key={order.id} as="li">
              <Row>{order.buyer} ({order.platform})</Row>
              <Row>
                {this.renderButton(order, "completed", "Teljesítve")}
                {this.renderDetailsButton(order.id)}
                {this.renderDeleteButton(order.id)}
              </Row>
              <Row>
                <OrderForm finishedProducts={this.props.finishedProducts}
                  flavors={this.props.flavors} submitOrder={this.modifyOrder}
                  toggleLabel="Módosítás" order={order} />
              </Row>
              {this.renderDeleteForm(order.id)}
              {details}
            </ListGroup.Item>;
          })}
        </ListGroup>
      </div>;
    }

    return <div>
      <OrderForm finishedProducts={this.props.finishedProducts}
        flavors={this.props.flavors} submitOrder={this.props.createOrder}
        toggleLabel="Új megrendelés" order={this.getDefaultOrder()} />
      {content}
    </div>;
  }
}
