import { connect } from "react-redux";
import { fetchCategories } from "../../actions/category_actions.js";

import SelectCategoryView from "./select_category_view.jsx";

const mapStateToProps = (state) => {
  return {
    isLoading: state.loadingCategories,
    categories: Object.values(state.categories)
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    loadCategories: () => dispatch(fetchCategories())
  };
};

const SelectCategory = connect(
  mapStateToProps,
  mapDispatchToProps
)(SelectCategoryView);

export default SelectCategory;
