import React from "react";
import ListGroup from "react-bootstrap/ListGroup.js";
import Row from "react-bootstrap/Row.js";

export default class SelectFinishedProductView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    this.props.loadFinishedProducts(this.props.flavorId);
  }

  render() {
    let content;
    if(this.props.isLoading) {
      content = <p>Betöltés</p>;
    } else {
      content = <div>
        <Row>
          <img src={"/api" + this.props.flavor.pictures[0].thumb}
            alt="flavor" className="img-thumbnail" />
        </Row>
        <Row>
          <ListGroup as="ul">
            {this.props.finishedProducts.map((finProd) => {
              return <ListGroup.Item key={finProd.id} as="li"
                       onClick={() => this.props.selectFinishedProduct(finProd.id)}>
                  {finProd.cost}
                </ListGroup.Item>;
            })}
          </ListGroup>
        </Row>
      </div>
    }

    return content;
  }
}
