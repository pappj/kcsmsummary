import { connect } from "react-redux";
import { fetchProducts } from "../../actions/product_actions.js";

import SelectProductView from "./select_product_view.jsx";

const mapStateToProps = (state, ownProps) => {
  const category = state.categories[ownProps.categoryId];
  let flavors = [];
  if(category.products) {
    flavors = category.products.map((prodId) => {
      return state.products[prodId].flavors.map((flavId) => {
        return state.flavors[flavId];
      })
    }).flat();
  }

  return {
    isLoading: category.loadingProducts,
    flavors
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    loadProducts: (catId) => dispatch(fetchProducts(catId))
  };
};

const SelectProduct = connect(
  mapStateToProps,
  mapDispatchToProps
)(SelectProductView);

export default SelectProduct;
