import { connect } from "react-redux";
import { fetchFinishedProducts } from "../../actions/finished_product_actions.js";

import SelectFinishedProductView from "./select_finished_product_view.jsx";

const mapStateToProps = (state, ownProps) => {
  const flavor = state.flavors[ownProps.flavorId];
  let finishedProducts = []
  if(flavor.finishedProducts) {
    finishedProducts = flavor.finishedProducts.map((prodId) => {
      return state.finishedProducts[prodId];
    });
  }

  return {
    isLoading: flavor.loadingFinishedProducts,
    finishedProducts,
    flavor
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    loadFinishedProducts: (flavorId) => dispatch(fetchFinishedProducts(flavorId))
  }
};

const SelectFinishedProduct = connect(
  mapStateToProps,
  mapDispatchToProps
)(SelectFinishedProductView);

export default SelectFinishedProduct;
