import React from "react";
import Row from "react-bootstrap/Row.js";
import Col from "react-bootstrap/Col.js";
import Button from "react-bootstrap/Button.js";

import SelectCategory from "./select_category.js";
import SelectProduct from "./select_product.js";
import SelectFinishedProduct from "./select_finished_product.js";

export default class ProductSelector extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selections: [{component: "Category"}]
    };

    this.back = this.back.bind(this);
    this.next = this.next.bind(this);
    this.getCurrentSelector = this.getCurrentSelector.bind(this);
  }

  back() {
    const newSelections = this.state.selections.slice(0, -1);

    this.setState({
      selections: newSelections
    });
  }

  next(component, prevId) {
    const newSelection = {
      component,
      prevId
    };

    this.setState({
      selections: [...this.state.selections, newSelection]
    });
  }

  getCurrentSelector() {
    const selection = this.state.selections[this.state.selections.length-1];

    switch(selection.component) {
      case "Category":
        return <SelectCategory next={this.next} />;
      case "Product":
        return <SelectProduct categoryId={selection.prevId} next={this.next} />;
      case "FinishedProduct":
        return <SelectFinishedProduct flavorId={selection.prevId}
                  selectFinishedProduct={this.props.selectFinishedProduct} />;
    }
  }

  render() {
    let backButton;
    if(this.state.selections.length > 1) {
      backButton = <Button variant="primary" onClick={this.back}>&lt; Vissza</Button>;
    }

    return <div>
      <Row>
        <Col sm="4">
          {backButton}
        </Col>
        <Col sm={{span: 4, offset: 4}}>
          <Button variant="primary" onClick={this.props.cancel}>Mégse</Button>
        </Col>
      </Row>
      <Row>
        {this.getCurrentSelector()}
      </Row>
    </div>;
  }
}
