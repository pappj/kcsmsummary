import React from "react";
import Row from "react-bootstrap/Row.js";
import Col from "react-bootstrap/Col.js";

export default class SelectProductView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    this.props.loadProducts(this.props.categoryId);
  }

  render() {
    let content;
    if(this.props.isLoading) {
      content = <p>Betöltés</p>;
    } else {
      content = <Row className="justify-content-around">
        {this.props.flavors.map((flavor) => {
          return <Col key={flavor.id}>
            <img src={"/api" + flavor.pictures[0].thumb}
              alt={"flavor"+flavor.id} className="img-thumbnail"
              onClick={() => this.props.next("FinishedProduct", flavor.id)} />
          </Col>
        })}
      </Row>;
    }

    return content;
  }
}
