import React from "react";
import ListGroup from "react-bootstrap/ListGroup.js";

export default class SelectCategoryView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    this.props.loadCategories();
  }

  render() {
    let content;

    if(this.props.isLoading) {
      content = <p>Betöltés</p>;
    } else {
      content = <ListGroup as="ul">
        {this.props.categories.map((cat) => {
            return <ListGroup.Item key={"cat"+cat.id} as="li"
                     onClick={() => this.props.next("Product", cat.id)}>{cat.name}
                   </ListGroup.Item>;
          })
        }
      </ListGroup>;
    }

    return content;
  }
}
