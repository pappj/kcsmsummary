import React from "react";
import ReactDOM from "react-dom";
import {NavLink} from "react-router-dom";
import Nav from "react-bootstrap/Nav.js";
import Navbar from "react-bootstrap/Navbar.js";

export default class NavBar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return <Navbar variant="dark" bg="dark" expand="lg">
      <Navbar.Toggle aria-controls="navbarNav" />
      <Navbar.Collapse id="navbarNav">
        <Nav as="ul">
          <Nav.Item as="li">
            <NavLink exact className="nav-link" to="/">Rendelések</NavLink>
          </Nav.Item>
          <Nav.Item as="li">
            <NavLink className="nav-link" to="/products">Termékek</NavLink>
          </Nav.Item>
          <Nav.Item as="li">
            <NavLink className="nav-link" to="/expenses">Kiadások</NavLink>
          </Nav.Item>
        </Nav>
      </Navbar.Collapse>
    </Navbar>;
  }
}
