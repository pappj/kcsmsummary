import React from "react";
import Row from "react-bootstrap/Row.js";
import Col from "react-bootstrap/Col.js";
import ListGroup from "react-bootstrap/ListGroup.js";
import Button from "react-bootstrap/Button.js";
import { translate } from "../common/expense_translator.js";

import ExpenseForm from "./expense_form.jsx";

export default class ExpensesView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showDeleteForm: {}
    };

    this.getDefaultExpense = this.getDefaultExpense.bind(this);
    this.showDeleteForm = this.showDeleteForm.bind(this);
    this.hideDeleteForm = this.hideDeleteForm.bind(this);
    this.renderExpenseRow = this.renderExpenseRow.bind(this);
    this.renderDeleteForm = this.renderDeleteForm.bind(this);
  }

  componentDidMount() {
    this.props.loadExpenses();
  }

  getDefaultExpense() {
    return {
      category: "commodity",
      date: undefined,
      amount: 0,
      note: ""
    };
  }

  showDeleteForm(expenseId) {
    this.setState({
      showDeleteForm: {...this.state.showDeleteForm,
                       [expenseId]: true}
    });
  }

  hideDeleteForm(expenseId) {
    this.setState({
      showDeleteForm: {...this.state.showDeleteForm,
                       [expenseId]: false}
    });
  }

  renderHeader() {
    return <ListGroup.Item key="-1">
      <Row>
        <Col key="1" sm="2" xs="3">Típus</Col>
        <Col key="2" sm="2" xs="3">Dátum</Col>
        <Col key="3" sm="2" xs="3">Ár</Col>
        <Col key="4" sm="2" xs="3">Megjegyzés</Col>
      </Row>
    </ListGroup.Item>;
  }

  renderExpenseRow(exp) {
    return <ListGroup.Item key={exp.id}>
      <Row>
        <Col key="1" sm="2" xs="3">{translate(exp.category)}</Col>
        <Col key="2" sm="2" xs="3">{exp.date}</Col>
        <Col key="3" sm="2" xs="3">{exp.amount} Ft</Col>
        <Col key="4" sm="2" xs="3">{translate(exp.note)}</Col>
        <Col key="5" sm="2" xs="3">
          <Button variant="danger"
            onClick={(evt) => {evt.preventDefault();
                               this.showDeleteForm(exp.id);}}>
            Törlés</Button>
        </Col>
      </Row>
      <Row>
        <ExpenseForm onSubmit={this.props.updateExpense}
          expense={exp} toggleLabel="Módosítás" />
      </Row>
      {this.renderDeleteForm(exp.id)}
    </ListGroup.Item>;
  }

  renderDeleteForm(expenseId) {
    let form;
    if(this.state.showDeleteForm[expenseId]) {
      form =  <Row>
        <p>Tényleg törlöd ezt a kiadást?</p>
        <Button variant="danger"
          onClick={(evt) => {evt.preventDefault();
                             this.hideDeleteForm(expenseId);
                             this.props.removeExpense(expenseId);}}>
          Igen</Button>
        <Button variant="primary"
          onClick={(evt) => {evt.preventDefault();
                             this.hideDeleteForm(expenseId);}}>
          Mégse</Button>
      </Row>;
    }

    return form;
  }

  render() {
    let content;
    if(this.props.isLoaded) {
      content = <section>
        <ExpenseForm onSubmit={this.props.createExpense}
          expense={this.getDefaultExpense()} toggleLabel="Új kiadás" />
        <ListGroup>
          {this.renderHeader()}
          {this.props.expenses.map((exp) => this.renderExpenseRow(exp))}
        </ListGroup>
      </section>;
    } else {
      content = <p>Betöltés</p>;
    }

    return content;
  }
}
