import React from "react";
import ToggleForm from "../common/toggle_form.jsx";
import { listToDictionary } from "../common/expense_translator.js";

export default class ExpenseForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};

    this.formFields = [
      { name: "category",
        type: "select",
        label: "Típus",
        options: listToDictionary([
                   "commodity",
                   "postage",
                   "wrapper",
                   "administrative",
                   "other"
                 ])
      },
      { name: "date",
        type: "date",
        label: "Dátum",
      },
      { name: "amount",
        type: "number",
        label: "Ár"
      },
      { name: "note",
        type: "dependentSelect",
        dependsOn: "category",
        label: "Megjegyzés",
        options: {
          commodity: listToDictionary([
                       "decorand",
                       "eurosuv",
                       "kosarbolt",
                       "kreativbirodalom",
                       "medves",
                       "qx",
                       "szarazvirag",
                       "viragtrend",
                       "other"]),
          postage: listToDictionary([
                     "foxpost",
                     "gls",
                     "other"]),
          wrapper: listToDictionary([
                     "remenyi",
                     "wanapack",
                     "other"]),
          administrative: listToDictionary([
                            "kata",
                            "szamla",
                            "accountant",
                            "ipa",
                            "other"]),
          other: listToDictionary(["other"])
        }
      }
    ];
  }

  render() {
    return <ToggleForm onSubmit={this.props.onSubmit} fields={this.formFields}
             instance={this.props.expense} submitLabel="Mentés"
             toggleLabel={this.props.toggleLabel} />;
  }
}
