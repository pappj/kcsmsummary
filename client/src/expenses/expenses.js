import { connect } from "react-redux";
import { fetchExpenses, createExpense, patchExpense, removeExpense }
  from "../actions/expense_actions.js";

import ExpensesView from "./expenses_view.jsx";

const mapStateToProps = (state) => {
  const sortedExpenses = Object.values(state.expenses).sort(
    (e1, e2) => e2.date.localeCompare(e1.date)
  );

  return {
    expenses: sortedExpenses,
    isLoaded: state.expensesLoaded
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    loadExpenses: () => dispatch(fetchExpenses()),
    createExpense: (exp) => dispatch(createExpense(exp)),
    updateExpense: (exp) => dispatch(patchExpense(exp)),
    removeExpense: (expId) => dispatch(removeExpense(expId))
  };
};

const Expenses = connect(
  mapStateToProps,
  mapDispatchToProps
)(ExpensesView);

export default Expenses;
