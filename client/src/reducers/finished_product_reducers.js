import { RECEIVE_FINISHED_PRODUCTS, RECEIVE_ONLY_FINISHED_PRODUCTS,
  ADD_FINISHED_PRODUCT, UPDATE_FINISHED_PRODUCT, UPDATE_FINISHED_PRODUCT_ORDERS }
  from "../actions/finished_product_actions.js";

export function finishedProducts(state = {}, action) {
  switch(action.type) {
    case RECEIVE_FINISHED_PRODUCTS:
      return {
        ...state,
        ...action.finishedProducts
      };
    case RECEIVE_ONLY_FINISHED_PRODUCTS:
      return {
        ...state,
        ...action.finishedProducts
      };
    case ADD_FINISHED_PRODUCT:
      return {
        ...state,
        [action.finishedProduct.id]: action.finishedProduct
      };
    case UPDATE_FINISHED_PRODUCT:
      return {
        ...state,
        [action.finishedProduct.id]: action.finishedProduct
      };
    case UPDATE_FINISHED_PRODUCT_ORDERS:
      let updatedState = {}
      action.addedProducts.forEach((id) => {
        updatedState[id] = {...state[id],
                            order_id: action.orderId};
      });
      action.removedProducts.forEach((id) => {
        updatedState[id] = {...state[id],
                            order_id: null};
      });

      return {...state,
              ...updatedState};
    default:
      return state;
  }
}
