import { REQUEST_CATEGORIES, RECEIVE_CATEGORIES, ADD_CATEGORY }
  from "../actions/category_actions.js";
import { REQUEST_PRODUCTS, RECEIVE_PRODUCTS, ADD_PRODUCT }
  from "../actions/product_actions.js";

export function loadingCategories(state = false, action) {
  switch(action.type) {
    case REQUEST_CATEGORIES:
      return true;
    case RECEIVE_CATEGORIES:
      return false;
    default:
      return state;
  }
}

export function categoriesLoaded(state = false, action) {
  switch(action.type) {
    case RECEIVE_CATEGORIES:
      return true;
    default:
      return state;
  }
}

export function categories(state = {}, action) {
  switch(action.type) {
    case RECEIVE_CATEGORIES:
      return {
        ...state,
        ...action.categories
      };
    case ADD_CATEGORY:
      return {
        ...state,
        [action.category.id]: action.category
      };
    case REQUEST_PRODUCTS:
      return {
        ...state,
        [action.categoryId]: {...state[action.categoryId], loadingProducts: true}
      };
    case RECEIVE_PRODUCTS:
      return {
        ...state,
        [action.categoryId]: {...state[action.categoryId], loadingProducts: false,
                              products: action.newIds}
      };
    case ADD_PRODUCT:
      const category = state[action.categoryId];
      let products = undefined;
      if(category.products) {
        products = [...category.products, action.product.id];
      }

      return {
        ...state,
        [action.categoryId]: {...category, products: products}
      };
    default:
      return state;
  }
}
