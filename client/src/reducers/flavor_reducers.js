import { RECEIVE_FLAVORS, ADD_FLAVOR } from "../actions/flavor_actions.js";
import { REQUEST_FINISHED_PRODUCTS, RECEIVE_FINISHED_PRODUCTS,
  ADD_FINISHED_PRODUCT } from "../actions/finished_product_actions.js";

export function flavors(state = {}, action) {
  let flavor;

  switch(action.type) {
    case RECEIVE_FLAVORS:
      return {
        ...state,
        ...action.flavors
      };
    case ADD_FLAVOR:
      return {
        ...state,
        [action.flavor.id]: action.flavor
      };
    case REQUEST_FINISHED_PRODUCTS:
      return {
        ...state,
        [action.flavorId]: {...state[action.flavorId],
                            loadingFinishedProducts: true}
      };
    case RECEIVE_FINISHED_PRODUCTS:
      flavor = state[action.flavorId];

      return {
        ...state,
        [action.flavorId]: {...flavor, loadingFinishedProducts: false,
                            finishedProducts: action.newIds}
      };
    case ADD_FINISHED_PRODUCT:
      flavor = state[action.flavorId];
      const finishedProducts = [...flavor.finishedProducts,
                                action.finishedProduct.id];

      return {
        ...state,
        [action.flavorId]: {...flavor, finishedProducts: finishedProducts}
      };
    default:
      return state;
  }
}

