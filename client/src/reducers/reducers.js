import { combineReducers } from "redux";

import { loadingCategories, categoriesLoaded, categories }
  from "./category_reducers.js";
import { products } from "./product_reducers.js";
import { flavors } from "./flavor_reducers.js";
import { finishedProducts } from "./finished_product_reducers.js";
import { loadingOrders, ordersLoaded, orders } from "./order_reducers.js";
import { loadingExpenses, expensesLoaded, expenses } from "./expense_reducers.js";

const rootReducer = combineReducers({
  loadingCategories,
  categoriesLoaded,
  categories,
  products,
  flavors,
  finishedProducts,
  loadingOrders,
  ordersLoaded,
  orders,
  loadingExpenses,
  expensesLoaded,
  expenses
});

export default rootReducer;
