import { REQUEST_PRODUCTS, RECEIVE_PRODUCTS, RECEIVE_SINGLE_PRODUCT, ADD_PRODUCT }
  from "../actions/product_actions.js";
import { ADD_FLAVOR } from "../actions/flavor_actions.js";

export function products(state = {}, action) {
  switch(action.type) {
    case RECEIVE_PRODUCTS:
      return {
        ...state,
        ...action.products
      };
    case RECEIVE_SINGLE_PRODUCT:
      return {
        ...state,
        [action.product.id]: action.product
      };
    case ADD_PRODUCT:
      return {
        ...state,
        [action.product.id]: action.product
      };
    case ADD_FLAVOR:
      const product = state[action.productId];
      const flavors = [...product.flavors, action.flavor.id];
      return {
        ...state,
        [action.productId]: {...product, flavors: flavors}
      };
    default:
      return state;
  }
}
