import { REQUEST_ORDERS, RECEIVE_ORDERS, ADD_ORDER, UPDATE_ORDER, DELETE_ORDER }
  from "../actions/order_actions.js";
import { UPDATE_FINISHED_PRODUCT }
  from "../actions/finished_product_actions.js";

export function loadingOrders(state = false, action) {
  switch(action.type) {
    case REQUEST_ORDERS:
      return true;
    case RECEIVE_ORDERS:
      return false;
    default:
      return state;
  }
}

export function ordersLoaded(state = false, action) {
  switch(action.type) {
    case RECEIVE_ORDERS:
      return true;
    default:
      return state;
  }
}

export function orders(state = {}, action) {
  switch(action.type) {
    case RECEIVE_ORDERS:
      return {
        ...state,
        ...action.orders
      };
    case ADD_ORDER:
      return {
        ...state,
        [action.order.id]: action.order
      };
    case UPDATE_ORDER:
      return {
        ...state,
        [action.order.id]: action.order
      };
    case DELETE_ORDER:
      let newState = {...state};
      delete newState[action.id];
      return newState;
    case UPDATE_FINISHED_PRODUCT:
      const product = action.finishedProduct;
      if(product.order_id) {
        const order = state[product.order_id];
        return {
          ...state,
          [order.id]: {...order,
                       ["products"]: [...order.products, product.id]}
        };
      } else {
        return state;
      }
    default:
      return state;
  }
}
