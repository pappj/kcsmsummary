import { REQUEST_EXPENSES, RECEIVE_EXPENSES, ADD_EXPENSE, UPDATE_EXPENSE,
         DELETE_EXPENSE }
  from "../actions/expense_actions.js";

export function expenses(state = {}, action) {
  switch(action.type) {
    case RECEIVE_EXPENSES:
      return {
        ...state,
        ...action.expenses
      };
    case ADD_EXPENSE:
      return {
        ...state,
        [action.expense.id]: action.expense
      };
    case UPDATE_EXPENSE:
      return {
        ...state,
        [action.expense.id]: action.expense
      };
    case DELETE_EXPENSE:
      let newState = Object.assign({}, state);
      delete newState[action.id];
      return newState;
    default:
      return state;
  }
}

export function loadingExpenses(state = false, action) {
  switch(action.type) {
    case REQUEST_EXPENSES:
      return true;
    case RECEIVE_EXPENSES:
      return false;
    default:
      return state;
  }
}

export function expensesLoaded(state = false, action) {
  switch(action.type) {
    case RECEIVE_EXPENSES:
      return true;
    default:
      return state;
  }
}

