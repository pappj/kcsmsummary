import React from "react";
import ListGroup from "react-bootstrap/ListGroup.js";
import Button from "react-bootstrap/Button.js";

export default class OrderSelector extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return <ListGroup>
      {this.props.orders.map((order) => {
        return <ListGroup.Item key={order.id}>
          <span>{order.buyer} ({order.platform})</span>
          <Button variant="primary"
            onClick={() => this.props.selectOrder(order.id)}>
            Kiválaszt</Button>
        </ListGroup.Item>;
      })}
    </ListGroup>;
  }
}
