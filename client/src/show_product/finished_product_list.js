import { connect } from "react-redux";
import { fetchOrders } from "../actions/order_actions.js";
import { patchFinishedProduct } from "../actions/finished_product_actions.js";

import FinishedProductListView from "./finished_product_list_view.jsx";

const mapStateToProps = (state, ownProps) => {
  let finishedProducts;
  if(state.flavors[ownProps.flavorId].finishedProducts) {
    finishedProducts = state.flavors[ownProps.flavorId].finishedProducts.map(
      (id) => state.finishedProducts[id])
  }

  return {
    finishedProducts,
    orders: state.orders
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    loadOrders: () => dispatch(fetchOrders()),
    updateProduct: (id, updateParams) =>
                     dispatch(patchFinishedProduct(id, updateParams))
  };
};

const FinishedProductList = connect(
  mapStateToProps,
  mapDispatchToProps
)(FinishedProductListView);

export default FinishedProductList;
