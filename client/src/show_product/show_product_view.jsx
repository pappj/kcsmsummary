import axios from "axios";
import React from "react";
import Row from "react-bootstrap/Row.js";
import Carousel from "react-bootstrap/Carousel.js";
import Image from "react-bootstrap/Image.js";

import ProductFlavorForm from "./product_flavor_form.jsx";
import FinishedProductForm from "./finished_product_form.jsx";
import FlavorList from "../products/flavor_list.js";
import FinishedProductList from "./finished_product_list.js";


export default class ShowProductView extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      flavorIndex: props.flavorIdx,
    };

    this.changeCurrentFlavor = this.changeCurrentFlavor.bind(this);
  }

  componentDidMount() {
    this.props.loadProduct(this.props.productId).then((response) => {
      this.props.loadFinishedProducts(
        this.props.flavors[this.state.flavorIndex].id);
    })
  }

  componentDidUpdate(prevProps, prevState) {
    if(prevState.flavorIndex != this.state.flavorIndex) {
      this.props.loadFinishedProducts(this.props.flavors[this.state.flavorIndex].id);
    }
  }

  changeCurrentFlavor(idx) {
    const params = new URLSearchParams();
    params.append("id", this.props.productId);
    params.append("flavorIdx", idx);
    window.history.pushState("", "", "/show_product?" + params);
    this.setState({flavorIndex: idx});
  }

  render() {
    let content;
    if(this.props.flavors) {
      const pictures = this.props.flavors[this.state.flavorIndex].pictures;
      const pictureIndexes = Array(pictures.length-1).fill(1).map((x, i) => i+1);
      const currentFlavor = this.props.flavors[this.state.flavorIndex];

      let finishedProductList;
      if(currentFlavor.loadingFinishedProducts) {
        finishedProductList = <p>Betöltés...</p>;
      } else {
        finishedProductList = <FinishedProductList flavorId={currentFlavor.id} />;
      }

      content = <div>
        <ProductFlavorForm productId={this.props.productId}
          createFlavor={this.props.createFlavor} />
        <Row>
          <h3>Alternatívák</h3>
          <FlavorList productId={this.props.productId}
            onClick={this.changeCurrentFlavor} />
        </Row>
        <Row>
          <Carousel>
            <Carousel.Item key={0}>
              <Image src={"/api"+pictures[0].orig} fluid />
            </Carousel.Item>
            {pictureIndexes.map((i) => {
              return <Carousel.Item key={i}>
                <Image src={"/api"+pictures[i].orig} fluid />
              </Carousel.Item>;
            })}
          </Carousel>
        </Row>
        <Row>
          <FinishedProductForm flavorId={currentFlavor.id}
            createFinishedProduct={this.props.createFinishedProduct} />
        </Row>
          {finishedProductList}
        <Row>
        </Row>
      </div>;
    } else {
      content = <p>Betöltés...</p>;
    }

    return <div>
      {content}
    </div>;

  }
}
