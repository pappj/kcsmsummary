import axios from "axios";
import React from "react";
import ToggleForm from "../common/toggle_form.jsx";

export default class FinishedProductForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};

    this.formFields = [
      { name: "cost",
        type: "number",
        label: "Költség"}
    ];

    this.createFinishedProduct = this.createFinishedProduct.bind(this);
  }

  createFinishedProduct(product) {
    const finishedProd = {
      ...product,
      product_flavor_id: this.props.flavorId
    };

    this.props.createFinishedProduct(finishedProd);
  }

  getDefaultFinishedProduct() {
    return { cost: 0 };
  }

  render() {
    return <ToggleForm onSubmit={this.createFinishedProduct}
             fields={this.formFields} submitLabel="Létrehozás"
             instance={this.getDefaultFinishedProduct()}
             toggleLabel="Új késztermék" />;
  }
}
