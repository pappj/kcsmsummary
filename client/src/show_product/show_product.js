import { connect } from "react-redux";
import { fetchSingleProduct } from "../actions/product_actions.js";
import { fetchFinishedProducts, createFinishedProduct }
  from "../actions/finished_product_actions.js";
import { createFlavor } from "../actions/flavor_actions.js";
import { getParamFromProps } from "../common/utilities.js";

import ShowProductView from "./show_product_view.jsx";

const mapStateToProps = (state, ownProps) => {
  const productId = getParamFromProps(ownProps, "id");
  const flavorIdx = getParamFromProps(ownProps, "flavorIdx");
  const product = state.products[productId];
  let flavors;
  if(product) {
    flavors = product.flavors.map(id => state.flavors[id]);
  }

  return {
    productId,
    flavorIdx,
    flavors
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    loadProduct: (id) => dispatch(fetchSingleProduct(id)),
    loadFinishedProducts: (flavorId) => dispatch(fetchFinishedProducts(flavorId)),
    createFlavor: (flavor) => dispatch(createFlavor(flavor)),
    createFinishedProduct: (product) => dispatch(createFinishedProduct(product))
  };
};

const ShowProduct = connect(
  mapStateToProps,
  mapDispatchToProps
)(ShowProductView);

export default ShowProduct;
