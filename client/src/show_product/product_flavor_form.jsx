import axios from "axios";
import React from "react";
import ToggleForm from "../common/toggle_form.jsx";

import {encodeBase64} from "../common/utilities.js";

export default class ProductFlavorForm extends React.Component {
  constructor(props) {
    super(props);

    this.state = {};

    this.formFields = [
      { name: "pictures",
        type: "images",
        label: "Képek" }
    ];

    this.createFlavor = this.createFlavor.bind(this);
  }

  createFlavor(flavor) {
    const encodedPicsPromises = flavor.pictures.map((file) => {
      return encodeBase64(file);
    });

    Promise.all(encodedPicsPromises).then((encodedPics) => {
      const flavorPictures = encodedPics.map((pic) => {
        return {picture: pic};
      });

      this.props.createFlavor({
        product_id: this.props.productId,
        product_pictures_attributes: flavorPictures
      });
    });
  }

  getDefaultFlavor() {
    return { pictures: [] };
  }

  render() {
    return <ToggleForm onSubmit={this.createFlavor} fields={this.formFields}
             instance={this.getDefaultFlavor()} submitLabel="Létrehozás"
             toggleLabel="Új alternatíva" />
  }
}
