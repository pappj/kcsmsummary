import React from "react";
import ListGroup from "react-bootstrap/ListGroup.js";
import Row from "react-bootstrap/Row.js";
import Col from "react-bootstrap/Col.js";
import Button from "react-bootstrap/Button.js";

import OrderDetails from "../orders/order_details.jsx";
import OrderSelector from "./order_selector.jsx";

export default class FinishedProductListView extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      showOrder: {},
      showOrderSelector: {},
      pendingOrders: []
    };

    this.renderOrderLabel = this.renderOrderLabel.bind(this);
    this.renderOrderDetails = this.renderOrderDetails.bind(this);
    this.renderOrderSelector = this.renderOrderSelector.bind(this);
    this.toggleShowOrder = this.toggleShowOrder.bind(this);
    this.toggleShowOrderSelector = this.toggleShowOrderSelector.bind(this);
    this.selectOrder = this.selectOrder.bind(this);
    this.separatePendingOrders = this.separatePendingOrders.bind(this);
  }

  componentDidMount() {
    this.props.loadOrders();
    this.separatePendingOrders(this.props.orders);
  }

  componentDidUpdate(prevProps) {
    if(prevProps != this.props) {
      this.separatePendingOrders(this.props.orders);
    }
  }

  separatePendingOrders(orders) {
    this.setState({
      pendingOrders: Object.values(orders).filter(order => !order.completed)
    });
  }

  toggleShowOrder(productId) {
    this.setState({
      showOrder: {...this.state.showOrder,
                  [productId]: !this.state.showOrder[productId]}
    });
  }

  toggleShowOrderSelector(productId) {
    this.setState({
      showOrderSelector: {
        ...this.state.showOrderSelector,
        [productId]: !this.state.showOrderSelector[productId]
      }
    });
  }

  selectOrder(productId, orderId) {
    this.props.updateProduct(productId, {order_id: orderId})
      .then((response) => {
        this.toggleShowOrderSelector(productId);
      });
  }

  renderOrderLabel(product) {
    let label;

    if(this.props.orders && product.order_id) {
      const order = this.props.orders[product.order_id];

      let detailsButtonLabel = "Részletek";
      if(this.state.showOrder[product.id]) {
        detailsButtonLabel = "Kevesebb";
      }

      label = [
        <span key={order.id + "_buyer"}>{order.buyer} ({order.platform}) </span>,
        <Button variant="info" key={order.id + "_details_button"}
          onClick={() => this.toggleShowOrder(product.id)}>
          {detailsButtonLabel}</Button>
      ]
    } else {
      let buttonLabel = "Megrendeléshez adás";
      if(this.state.showOrderSelector[product.id]) {
        buttonLabel = "Mégse";
      }

      label = [
        <span key="label">Még megvan</span>,
        <Button variant="info" key="button"
          onClick={() => this.toggleShowOrderSelector(product.id)}>
        {buttonLabel}</Button>
      ]
    }

    return label;
  }

  renderOrderDetails(product) {
    let details;
    if(this.props.orders && this.state.showOrder[product.id]) {
      details = <OrderDetails order={this.props.orders[product.order_id]} />
    }

    return details;
  }

  renderOrderSelector(productId) {
    let selector;
    if(this.props.orders && this.state.showOrderSelector[productId]) {
      selector = <OrderSelector orders={this.state.pendingOrders}
        cancel={() => this.toggleShowOrderSelector(productId)}
        selectOrder={(orderId) => this.selectOrder(productId, orderId)}/>;
    }

    return selector;
  }

  render() {
    let header = "";
    let finishedProducts = [];
    if(this.props.finishedProducts) {
      finishedProducts = this.props.finishedProducts;
    }

    if(finishedProducts.length > 0) {
      header = <ListGroup.Item as="li" key="head">
        <Row>
          <Col sm="6">Költség</Col>
          <Col sm="6">Megrendelés</Col>
        </Row>
      </ListGroup.Item>;
    }

    return <ListGroup as="ul">
      {header}
      {finishedProducts.map((prod) => {
        return <ListGroup.Item as="li" key={prod.id}>
          <Row>
            <Col sm="6">{prod.cost} Ft</Col>
            <Col sm="6">{this.renderOrderLabel(prod)}</Col>
            {this.renderOrderDetails(prod)}
            {this.renderOrderSelector(prod.id)}
          </Row>
        </ListGroup.Item>;
      })}
    </ListGroup>;
  }
}
