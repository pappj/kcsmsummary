import axios from "axios";
import React from "react";

import CategoryForm from "../products/category_form.jsx";
import CategoryList from "../products/category_list.jsx";
import ProductForm from "../products/product_form.jsx";

export default class ProductsView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    this.props.loadCategories();
  }

  render() {
    let content;
    if(this.props.isLoading) {
      content = <p>Betöltés</p>;
    } else {
      content = <div>
        <div className="row">
          <CategoryForm addedNewCategory={this.props.addCategory} />
          <ProductForm categories={this.props.categories}
            createProduct={this.props.createProduct} />
        </div>
        <h1>Kategóriák</h1>
        <CategoryList categories={this.props.categories} />
      </div>
    }

    return <div>
      {content}
    </div>;
  }
}
