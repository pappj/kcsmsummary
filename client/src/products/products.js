import { connect } from "react-redux";
import { fetchCategories, addCategory } from "../actions/category_actions.js";
import { createProduct } from "../actions/product_actions.js";

import ProductsView from "./products_view.jsx";

const mapStateToProps = state => {
  return {
    isLoading: state.loadingCategories,
    categories: Object.values(state.categories)
  };
};

const mapDispatchToProps = dispatch => {
  return {
    loadCategories: () => dispatch(fetchCategories()),
    addCategory: (category) => dispatch(addCategory(category)),
    createProduct: (product) => dispatch(createProduct(product))
  };
};

const Products = connect(
  mapStateToProps,
  mapDispatchToProps
)(ProductsView);

export default Products;
