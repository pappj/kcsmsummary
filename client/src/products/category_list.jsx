import React from "react";
import { FontAwesomeIcon as Icon } from "@fortawesome/react-fontawesome";
import ListGroup from "react-bootstrap/ListGroup.js";
import Row from "react-bootstrap/Row.js";

import ProductList from "./product_list.js";

export default class CategoryList extends React.Component {
  constructor(props) {
    super(props);
    const falseArray = Array(props.categories.length).fill(false);
    this.state = {
      showProducts: falseArray
    }

    this.toggleShowProducts = this.toggleShowProducts.bind(this);
  }

  toggleShowProducts(idx) {
    let newArray = this.state.showProducts.slice();
    newArray[idx] = !newArray[idx];
    this.setState({showProducts: newArray});
  }

  render() {
    return <ListGroup as="ul">
      {this.props.categories.map((cat, idx) => {
        let iconName = "angle-right";
        let prodList = "";
        if(this.state.showProducts[idx]) {
          iconName = "angle-down";
          prodList = <Row>
            <ProductList categoryId={cat.id} />
          </Row>;
        }
        return <ListGroup.Item key={idx} >
          <Row>
            <ListGroup.Item action as="li"
              onClick={() => this.toggleShowProducts(idx)}>
              <Icon icon={iconName}></Icon> {cat.name}
            </ListGroup.Item>
          </Row>
          {prodList}
        </ListGroup.Item>;
      })}
    </ListGroup>;
  }
}
