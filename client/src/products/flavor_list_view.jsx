import React from "react";

export default class FlavorList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return <ul className="flavor-list">
      {this.props.flavors.map((flavor, i) => {
        if(flavor.pictures.length > 0) {
          return <li key={i} onClick={() => this.props.onClick(i)}>
            <img src={"/api" + flavor.pictures[0].thumb} alt={"flavor"+i}
              className="img-thumbnail"/>
          </li>
        }

        return "";
      })}
    </ul>;
  }
}
