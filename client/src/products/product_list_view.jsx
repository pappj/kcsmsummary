import React from "react";
import { withRouter } from "react-router";
import ListGroup from "react-bootstrap/ListGroup.js";

import FlavorList from "./flavor_list.js";

class ProductListView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    this.props.loadProducts(this.props.categoryId);
  }

  showProduct(productId, flavorIdx) {
    const params = new URLSearchParams();
    params.append("id", productId);
    params.append("flavorIdx", flavorIdx);
    this.props.history.push("/show_product?" + params);
  }

  render() {
    let content;
    const products = this.props.products;

    if(this.props.isLoading) {
      content = <ListGroup.Item as="li" key="loading">
        Betöltés
      </ListGroup.Item>
    } else if(!products || products.length == 0) {
      content = <ListGroup.Item as="li" key="noProd">
        <span className="align-middle">Nincsenek termékek ebben a kategóriában</span>
      </ListGroup.Item>
    } else {
      content = products.map((prod, idx) => {
        return <ListGroup.Item as="li" key={idx} action>
          <FlavorList productId={prod.id}
            onClick={(i) => this.showProduct(prod.id, i)} />
        </ListGroup.Item>
      })
    }

    return <ListGroup as="ul">
      {content}
    </ListGroup>;
  }
}

export default withRouter(ProductListView);
