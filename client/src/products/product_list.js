import { connect } from "react-redux";
import { fetchProducts } from "../actions/product_actions.js";

import ProductListView from "./product_list_view.jsx";

const mapStateToProps = (state, ownProps) => {
  const category = state.categories[ownProps.categoryId];
  let prods = undefined;
  if(category.products) {
    prods = category.products.map(id => {
      return state.products[id];
    });
  }
  return {
    isLoading: category.loadingProducts,
    products: prods
  };
};

const mapDispatchToProps = dispatch => {
  return {
    loadProducts: (categoryId) => dispatch(fetchProducts(categoryId))
  };
};

const ProductList = connect(
  mapStateToProps,
  mapDispatchToProps
)(ProductListView);

export default ProductList;
