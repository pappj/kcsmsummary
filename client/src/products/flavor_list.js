import { connect } from "react-redux";
import { getParamFromProps } from "../common/utilities.js";

import FlavorListView from "./flavor_list_view.jsx";

const mapStateToProps = (state, ownProps) => {
  const product = state.products[ownProps.productId];
  let flavors;
  if(product) {
    flavors = product.flavors.map(id => state.flavors[id]);
  }

  return {
    flavors
  };
};

const FlavorList = connect(
  mapStateToProps,
  null
)(FlavorListView);

export default FlavorList;
