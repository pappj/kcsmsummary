import axios from "axios";
import React from "react";
import ToggleForm from "../common/toggle_form.jsx";

export default class CategoryForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};

    this.formFields = [
      { name: "name",
        type: "text",
        label: "Kategória neve"}
    ];

    this.createCategory = this.createCategory.bind(this);
  }

  getDefaultCategory() {
    return {name: ""}
  }

  createCategory(category) {
    axios.post("/api/product_categories", {
      product_category: category
    })
      .then((response) => {
        this.props.addedNewCategory(response.data);
        this.setState({
          newCategory: {
            name: ""
          }
        });
      });
  }

  render() {
    return <ToggleForm onSubmit={this.createCategory} fields={this.formFields}
             instance={this.getDefaultCategory()} submitLabel="Létrehozás"
             toggleLabel="Új kategória" />;
  }
}
