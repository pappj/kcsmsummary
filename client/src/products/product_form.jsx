import axios from "axios";
import React from "react";
import ToggleForm from "../common/toggle_form.jsx";

import {encodeBase64} from "../common/utilities.js";

export default class ProductForm extends React.Component {
  constructor(props) {
    super(props);

    this.state = {};

    this.formFields = [
      { name: "category",
        type: "select",
        label: "Kategória",
        options: props.categories.reduce((obj, cat) => {
          return {...obj, [cat.id]: cat.name};
        }, {})},
      { name: "pictures",
        type: "images",
        label: "Képek"}
    ];

    this.createProduct = this.createProduct.bind(this);
  }


  createProduct(product) {
    const encodedPicsPromises = product.pictures.map((file) => {
      return encodeBase64(file);
    });

    Promise.all(encodedPicsPromises).then((encodedPics) => {
      const productPictures = encodedPics.map((pic) => {
        return {picture: pic};
      });

      this.props.createProduct({
        name: product.name,
        product_category_id: product.category,
        product_flavors_attributes: [{
          product_pictures_attributes: productPictures
        }]
      });
    });
  }

  getDefaultNewProduct(categories) {
    let firstCategory = "";
    if(categories && categories.length > 0) {
      firstCategory =  categories[0].id;
    }

    return {
      name: "",
      category: firstCategory,
      pictures: []
    }
  }

  render() {
    return <ToggleForm onSubmit={this.createProduct} fields={this.formFields}
             instance={this.getDefaultNewProduct(this.props.categories)}
             submitLabel="Létrehozás" toggleLabel="Új termék" />
  }
}
