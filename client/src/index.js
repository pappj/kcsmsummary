import React from "react";
import ReactDOM from "react-dom";
import {Route, Switch, BrowserRouter as Router} from "react-router-dom";

import { createStore, applyMiddleware } from "redux";
import thunkMiddleware from "redux-thunk";
import rootReducer from "./reducers/reducers.js";
import { Provider } from "react-redux";
import { composeWithDevTools } from "redux-devtools-extension";

import "bootstrap/dist/css/bootstrap.min.css";
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faAngleRight, faAngleDown } from '@fortawesome/free-solid-svg-icons'
import Container from "react-bootstrap/Container.js";

import NavBar from "./packs/navbar.jsx";
import Orders from "./orders/orders.js";
import Products from "./products/products.js";
import ShowProduct from "./show_product/show_product.js";
import Expenses from "./expenses/expenses.js";
import "./css/kcsm.css";

const store = createStore(rootReducer, composeWithDevTools(
  applyMiddleware(thunkMiddleware)));

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return <Provider store={store}>
      <Router>
        <Container>
          <NavBar />

          <Switch>
            <Route path="/" exact render={props => <Orders {...props} />} />
            <Route path="/products" render={props => <Products {...props} />} />
            <Route path="/show_product" render={props => <ShowProduct {...props} />} />
            <Route path="/expenses" render={props => <Expenses {...props} />} />
          </Switch>
        </Container>
      </Router>
    </Provider>;
  }
}

library.add(faAngleRight);
library.add(faAngleDown);

ReactDOM.render(<App />, document.getElementById("react"));
