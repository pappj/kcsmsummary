const resolve = require("path").resolve;
const HtmlWebPackPlugin = require("html-webpack-plugin");

const htmlPlugin = new HtmlWebPackPlugin({
  template: "./src/index.html",
  filename: "./index.html"
});

module.exports = {
    entry: "./src/index.js",
    module: {
        rules: [
            {
                test: /\.jsx?$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader"
                }
            },
            {
                test: /\.css/,
                use: ["style-loader", "css-loader"]
            }
        ]
    },
    devServer: {
        host: '0.0.0.0',
        port: 3000,
        publicPath: "/"
    },
    output: {
      path: resolve("dist"),
      filename: "bundle.js",
      publicPath: "/static/"
    },
    plugins: [htmlPlugin],
};

